import {
    FETCH_CARS_SUCCESS,
    FETCH_CARS_LOADING,
    FETCH_CARS_FAIL,
    FILTER_CARS
} from '../actions/type.actions'

const cars = JSON.parse(localStorage.getItem("cars"))
const initialState = cars 
                    ? { loading: false, error: false, errorMessage: "", cars: cars, filteredCars: [] } 
                    : { loading: false, error: false, errorMessage: "", cars: null, filteredCars: null }

export default function (state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case FETCH_CARS_SUCCESS:
            const getRandomInt = (min, max) => {
                min = Math.ceil(min);
                max = Math.floor(max);
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            
            const results = []
            for (let i = 0; i < payload.cars.length; i++){
                const car = payload.cars[i]
                const isPositive = getRandomInt(0, 1) === 1;
                const timeAt = new Date();
                const mutator = getRandomInt(1000000, 100000000);
                let availableAt = new Date(timeAt.getTime() + (isPositive ? mutator : -1 * mutator))
                const withDriver = Math.round(Math.random() * 1) === 1

                results.push({
                    ...car,
                    availableAt,
                    withDriver
                })
            }
            return {
                ...state,
                loading: false,
                error: false,
                errorMessage: "",
                cars: results
            }
        case FETCH_CARS_LOADING:
            return {
                ...state,
                loading: true,
                error: false,
                errorMessage: ""
            };

        case FETCH_CARS_FAIL:
            return {
                ...state,
                loading: false,
                error: true,
                errorMessage: payload.errorMessage
            }

        case FILTER_CARS:
            const { type, date, time, passenger } = payload
            let cars = state.cars
            
            if (type){
                cars = cars.filter(car => car.withDriver === (type === "Dengan Driver"))
            }
            if (date && time){
                cars = cars.filter(car => {
                    const dataAvailableAt = new Date(car.availableAt).getTime()
                    const filterAvailableAt = new Date(`${date} ${time}`).getTime()

                    return dataAvailableAt >= filterAvailableAt
                })
            }

            if (passenger){
                cars = cars.filter(car => car.capacity >= Number(passenger))
            }

            return {
                ...state,
                filteredCars: cars
            }

        default:
            return state;
    }
}
