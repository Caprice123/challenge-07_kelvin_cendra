import {
    LOGIN_SUCCESS,
    LOGIN_FAIL, 
    LOGOUT_SUCCESS,
    LOGOUT_FAIL
} from '../actions/type.actions'

const tokenResponse = localStorage.getItem("tokenResponse")
const user = localStorage.getItem("user")

const initialState = (tokenResponse && user)
                        ? { token: tokenResponse, user: user, isLoggedIn: true, error: false, errorMessage: null }
                        : { token: null, user: null, isLoggedIn: false, error: false, errorMessage: null }

export default function (state = initialState, action){
    const { type, payload } = action

    switch (type){
        case LOGIN_SUCCESS:
            return {
                ...state,
                token: payload.tokenResponse,
                user: payload.user,
                isLoggedIn: true,
                error: false,
                errorMessage: null
            }
        case LOGIN_FAIL:
            return {
                ...state,
                error: true,
                errorMessage: payload.errorMessage
            }
        case LOGOUT_SUCCESS:
            return {
                ...state,
                token: null,
                user: null,
                isLoggedIn: false,
                error: false,
                errorMessage: null
            }
        case LOGOUT_FAIL:
            return {
                ...state,
                error: true,
                errorMessage: payload.errorMessage
            }
        default:
            return state
    }
}


