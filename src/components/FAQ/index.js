import React from 'react'

// components
import Accordions from '../Accordions'

// styles
import { Wrapper, Content } from './FAQ.styles'

// data faq
import { AccordionList } from './FAQ.data'


const FAQ = () => {
    return (
        <Wrapper id="faq">
            <Content md={5}  className="my-3">
                <h2>Frequently Asked Question</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia, qui.</p>
            </Content>
            <Content md={7}>
                <Accordions data={AccordionList} />
            </Content>
        </Wrapper>
    )
}

export default FAQ