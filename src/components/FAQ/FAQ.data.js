export const AccordionList = [
    {
        question: "Apa saja syarat yang dibutuhkan?",
        answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, ex."
    },
    {
        question: "Berapa hari minimal sewa mobil lepas kunci?",
        answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, ex."
    },
    {
        question: "Berapa hari sebelumnya sebaiknya booking sewa mobil?",
        answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, ex."
    },
    {
        question: "Apakah Ada biaya antar-jemput?",
        answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, ex."
    },
    {
        question: "Bagaimana jika terjadi kecelakaan?",
        answer: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, ex."
    },
]