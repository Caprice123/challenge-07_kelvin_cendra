import styled from "styled-components";

import { Row, Col } from 'react-bootstrap'

export const Wrapper = styled(Row)`
    padding-top: var(--navbar-height) !important;
    width: 90%;
    margin: 0 auto;
`

export const Content =  styled(Col)`

`

