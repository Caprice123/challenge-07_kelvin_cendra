
import Person1 from '../../images/img_photo.svg'
import Person2 from '../../images/img_photo2.svg'


export const testimonialInfos = [
    {
        img: {
            src: Person1,
            alt: "person 1"
        }, 
        desc: `"Lorem ipsum dolor sit amet consectetur,
        adipisicing elit. Numquam doloribus doloremque ullam voluptatibus! Consectetur optio
        cum a atque aut molestias asperiores voluptas. Laboriosam sapiente animi excepturi
        modi enim ullam debitis id suscipit ducimus odit! Unde numquam, voluptatum iusto
        vero ducimus cum possimus dignissimos veniam velit sint ad omnis accusamus
        alias."`,
        name: "John Dee 32, Bromo"
    }, {
        img: {
            src: Person2,
            alt: "person 2"
        }, 
        desc: `"Lorem ipsum dolor sit amet consectetur,
        adipisicing elit. Numquam doloribus doloremque ullam voluptatibus! Consectetur optio
        cum a atque aut molestias asperiores voluptas. Laboriosam sapiente animi excepturi
        modi enim ullam debitis id suscipit ducimus odit! Unde numquam, voluptatum iusto
        vero ducimus cum possimus dignissimos veniam velit sint ad omnis accusamus
        alias."`,
        name: "John Dee 32, Bromo"
    }
]
