import React from 'react'

// styles
import { Wrapper, Content, Title, Description, RentBtn } from './Banner.styles'

const Banner = () => {
    return (
        <Wrapper>
            <Content className="mx-auto">
                <Title className="mx-auto">Sewa Mobil di Cengkareng Sekarang</Title>
                <Description className="mx-auto">
                    Lorem ipsum dolor sit amet consectetur
                    adipisicing elit. Animi atque recusandae, facere similique at quos.
                </Description>
                <RentBtn variant="success" className="mx-auto">Mulai Sewa Mobil</RentBtn>
            </Content>
        </Wrapper>
    )
}

export default Banner