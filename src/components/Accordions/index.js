import React from 'react'

// styles
import { Wrapper, Content, Question, Answer } from './Accordions.styles'

const Accordions = ({ data }) => {
    return (
        <Wrapper>
            {
                data.map(({ question, answer }, index) => (
                    <Content key={index} eventKey={index} className="my-3">
                        <Question>{ question }</Question>
                        <Answer>{ answer }</Answer>
                    </Content>
                ))
            }
        </Wrapper>
    )
}

export default Accordions