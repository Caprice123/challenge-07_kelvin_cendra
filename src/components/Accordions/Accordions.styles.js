import styled from "styled-components";

import { Accordion } from 'react-bootstrap'

export const Wrapper = styled(Accordion)`

`

export const Content = styled(Accordion.Item)`
    border-top: 1px solid rgba(0, 0, 0, 0.125) !important;
`

export const Question = styled(Accordion.Header)`

`

export const Answer = styled(Accordion.Body)`
`
