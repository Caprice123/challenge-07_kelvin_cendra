import React from 'react'

// components
import { GoogleLogin } from 'react-google-login'

// config
import { clientId } from '../../config/googleOAuth'

// styles
import { LoginBtn } from './LoginWithGoogle.styles'

// redux state management
import { useDispatch } from 'react-redux'
import { setUser, setUserError } from '../../actions/auth.actions'


const LoginWithGoogle = () => {
    const dispatch = useDispatch()

    const onSuccess = (res) => {
        const { accessToken } = res
        console.log(accessToken)
        dispatch(setUser(accessToken))
    }

    const onFailure = (res) => {
        dispatch(setUserError(res.error))
    }


    return (
        <GoogleLogin
            id="test"
            render={renderProps => (
                <LoginBtn variant="success" onClick={renderProps.onClick} className="py-2">Login with Google</LoginBtn>
            )}
            isSignedIn={true}
            clientId={clientId}
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={'single_host_origin'}
            
        />
    )
}

export default LoginWithGoogle