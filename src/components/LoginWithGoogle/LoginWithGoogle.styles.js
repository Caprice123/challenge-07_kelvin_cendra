import styled from 'styled-components'

import { Button } from 'react-bootstrap'

export const LoginBtn = styled(Button)`
    border: none;
    outline: none;
`
