import CompleteIcon from '../../images/icon_complete.svg'
import PriceIcon from '../../images/icon_price.svg'
import HoursIcon from '../../images/icon_24hours.svg'
import ProfessionalIcon from '../../images/icon_professional.svg'

export const whyUsCards = [
    {
        img: {
            src: CompleteIcon,
            alt: "complete icon"
        },
        title: "Mobil Lengkap",
        description: "Tersedia banyak pilihan mobil, kondisi masih baru, bersih, dan terawat."
    },
    {
        img: {
            src: PriceIcon,
            alt: "price icon"
        },
        title: "Harga Murah",
        description: "Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain."
    },
    {
        img: {
            src: HoursIcon,
            alt: "24 hours icon",
        },
        title: "Layanan 24 Jam",
        description: "Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu."
    },
    {
        img: {
            src: ProfessionalIcon,
            alt: "supir professional icon"
        },
        title: "Supir Profesional",
        description: "Sopir yang profesional, berpengalaman, jujur, ramah, dan selalu tepat waktu."
    },
]