import React from 'react'

// components
import Card from '../Card'

// styles
import { Wrapper, Content } from './WhyUs.styles'

// data why us
import { whyUsCards } from './WhyUs.data'

const WhyUs = () => (
    <Wrapper id="why-us" className="py-5">
        <Content>
            <h2>Why Us?</h2>
        </Content>

        <Content>
            <p>Mengapa harus memilih Binar Car Rental?</p>
        </Content>
        
        <Content>
            { whyUsCards.map(({ img, title, description }) => (
                <Card key={title} styles={{width: "20rem", height: "16rem"}}>
                    <img src={img.src} alt={img.alt} className="icon" />
                    <h4>{title}</h4>
                    <p>{description}</p>
                </Card>
            ))}
        </Content>
    </Wrapper>
)


export default WhyUs