import styled from "styled-components";

import { Row, Container } from 'react-bootstrap'

export const Wrapper = styled(Container)`
    max-width: none;
    padding-top: var(--navbar-height) !important;
`

export const Content = styled(Row)`
    padding: 0 5%;
`


