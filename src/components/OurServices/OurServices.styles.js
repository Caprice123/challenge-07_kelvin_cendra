import styled from "styled-components";

import { Row, Col } from 'react-bootstrap'


export const Wrapper = styled(Row)`
    padding-top: var(--navbar-height) !important;
    width: 90%;
    margin: 0 auto;
    justify-content: space-between;
    max-width: none;
`


export const Content = styled(Col)`
    margin: auto;
    padding: 0 25px;
    
    ul{
        padding: 0;
        list-style-type: none;
    }
    img{
        width: initial;
        margin: 2.5px 25px 2.5px 0;

        &.our-service-image{
            width: 87.5%;
        }
    }
`
