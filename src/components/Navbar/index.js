import React from 'react'

// components
import LoginWithGoogle from '../LoginWithGoogle'
import LogoutWithGoogle from '../LogoutWithGoogle'
import { Dropdown } from 'react-bootstrap'

// styles
import { Wrapper, Content, ToggleBtn, Link, Brand, RegisterBtn } from './Navbar.styles'

// redux state management
import { useSelector } from 'react-redux'

const Navigation = () => {
    const { isLoggedIn, user, error, errorMessage } = useSelector(state => state.auth)

    return (
        <Wrapper className="mx-auto" fixed="top" expand="lg">
            <Brand href="/" />
            <ToggleBtn aria-controls="basic-navbar-nav" />
            <Content id="basic-navbar-nav">
                <Link to="/" className="mx-2">Our Services</Link>
                <Link to="/" className="mx-2">Why Us</Link>
                <Link to="/" className="mx-2">Testimonial</Link>
                <Link to="/" className="mx-2">FAQ</Link>
                
                { !error && isLoggedIn ? (
                    <>
                        <Dropdown className="mx-2 logout">
                            <Dropdown.Toggle variant="success" id="dropdown-basic">
                                { user.name }
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                                <Dropdown.Item>
                                    <LogoutWithGoogle />
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </>
                    
                ) : ( 
                    <Link className="mx-2 login">
                        <LoginWithGoogle />
                    </Link>
                // <RegisterBtn variant="success">Register</RegisterBtn>
                ) }
                
                { error && errorMessage !== "popup_closed_by_user" && <p>{ errorMessage }</p> }
            </Content>
        </Wrapper>
    )
}

export default Navigation

