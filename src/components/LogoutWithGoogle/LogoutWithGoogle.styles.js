import styled from "styled-components";
import { Button } from 'react-bootstrap'


export const LogoutBtn = styled(Button)`
    width: 100%;
    text-align: center;
    color: white;
    border: none;
    outline: none;
`
