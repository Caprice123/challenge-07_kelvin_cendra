import React from 'react'

// config
import { clientId } from '../../config/googleOAuth'

// components
import { GoogleLogout } from 'react-google-login'

// styles
import { LogoutBtn } from './LogoutWithGoogle.styles'

// redux state management
import { useDispatch } from 'react-redux'
import { unsetUser, unsetUserError } from '../../actions/auth.actions'

const LogoutWithGoole = () => {
    const dispatch = useDispatch()

    const onSuccess = () => {
        dispatch(unsetUser())
    }
    const onFailure = (res) => {
        dispatch(unsetUserError(res.error))
    }
    return (
        <GoogleLogout 
            render={renderProps => (
                <LogoutBtn variant="success" onClick={renderProps.onClick} className="py-2">Logout</LogoutBtn>
            )}
            clientId={clientId}
            buttonText="Logout"
            onFailure={onFailure}
            onLogoutSuccess={onSuccess}
            />
    )
}

export default LogoutWithGoole