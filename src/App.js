import React, { useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

// pages
import Home from './pages/Home';
import Rent from './pages/Rent';

// styling
import GlobalStyle from './GlobalStyle';
import { gapi } from 'gapi-script'

const App = () => {

    useEffect(() => {
        function start() {
            gapi.client.init({
                clientId: "199212012863-pd27fu47b6qg5o1ash2e1jer2hj6bsfd.apps.googleusercontent.com",
                scope: ""
                // client
            })
        }

        gapi.load("client:auth2", start)
    })

    return (
        <Router>
            <Routes>
                <Route exact path='/' element={<Home />} />
                <Route path='/rent' element={<Rent />} />
            </Routes>

            <GlobalStyle />
        </Router>
    );
}

export default App;
