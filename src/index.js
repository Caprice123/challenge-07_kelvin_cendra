import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import { GoogleOAuthProvider } from '@react-oauth/google';

import {Provider} from 'react-redux'
import store from './store'

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        {/* <GoogleOAuthProvider clientId='199212012863-pd27fu47b6qg5o1ash2e1jer2hj6bsfd.apps.googleusercontent.com'> */}
            <Provider store={store}>
                <App />
            </Provider>
        {/* </GoogleOAuthProvider> */}
    </React.StrictMode>
);

