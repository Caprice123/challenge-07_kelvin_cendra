import axios from 'axios'

export const getUserInfo = async (accessToken) => {
    const response = await axios.get(`https://www.googleapis.com/oauth2/v1/userinfo`, { headers: {
            'Authorization': 'Bearer ' + accessToken
        }
    })

    const user = response.data
    localStorage.setItem("accessToken", JSON.stringify(accessToken))
    localStorage.setItem("user", JSON.stringify(user))
    return user
}

export const removeUser = () => {
    localStorage.removeItem("user")
    localStorage.removeItem("tokenResponse")
}

