export const FETCH_CARS_SUCCESS = "FETCH_CARS_SUCCESS"
export const FETCH_CARS_LOADING = "FETCH_CARS_PENDING"
export const FETCH_CARS_FAIL = "FETCH_CARS_FAIL"

export const FILTER_CARS = "FILTER_CARS"



export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_FAIL = "LOGIN_FAIL"

export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS"
export const LOGOUT_FAIL = "LOGOUT_FAIL" 
