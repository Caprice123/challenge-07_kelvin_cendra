import {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    LOGOUT_FAIL
    
} from './type.actions'

import { getUserInfo, removeUser } from '../services/auth.services'

export const setUser = (accessToken) => async (dispatch) => {
    try{
        const user = await getUserInfo(accessToken)

        await dispatch({
            type: LOGIN_SUCCESS,
            payload: { accessToken, user }
        })
    } catch(err){
        await dispatch({
            type: LOGIN_FAIL,
            payload: { errorMessage: err.message }
        })
    }
}

export const setUserError = (errorMessage) => ({
    type: LOGIN_FAIL,
    payload: { errorMessage }
})

export const unsetUser = () => async (dispatch) => {
    try{
        removeUser()

        await dispatch({
            type: LOGOUT_SUCCESS
        })
    } catch (err){
        await dispatch({
            type: LOGOUT_FAIL,
            payload: { errorMessage: err.message }
        })
    }
}

export const unsetUserError = (errorMessage) => ({
    type: LOGOUT_FAIL,
    payload: { errorMessage }
})

